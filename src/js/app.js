import './utils'

const id = $('main').attr('id')

switch (id) {
  case 'home-page':
    require('./pages/home')
    break
  case 'category-page':
    require('./pages/category')
    break
  case 'sub-category-page':
    require('./pages/subcategory')
    break
  case 'thank-page':
    require('./pages/thank')
}
