function initSubcategorySliderBig(id) {
  const $subcategorySliderBig = $(`#sub-category-slider_big_${id}`)
  
  $subcategorySliderBig.slick({
    speed: 500,
    dots: false,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: `#sub-category-slider_small_${id}`
  })
}

function initSubcategorySliderSmall(id) {
  const $subcategorySliderSmall = $(`#sub-category-slider_small_${id}`)
  
  $subcategorySliderSmall.slick({
    speed: 500,
    dots: false,
    arrows: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    focusOnSelect: true,
    asNavFor: `#sub-category-slider_big_${id}`
  })
}

$(document).ready(function () {
  $('.sub-category-slider_big').each((key, el) => {
    const $slider = $(el)
    $slider.attr('id', `sub-category-slider_big_${key}`)
    initSubcategorySliderBig(key)
  })
  
  $('.sub-category-slider_small').each((key, el) => {
    const $slider = $(el)
    $slider.attr('id', `sub-category-slider_small_${key}`)
    initSubcategorySliderSmall(key)
  })
})
