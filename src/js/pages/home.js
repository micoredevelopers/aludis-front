function initMainSlider() {
  $('#main-slider').slick({
    speed: 700,
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1
  })
}

export function initStockSlider() {
  const $stockSlider = $('#stock-slider')
  const $currentSlide = $('.stock-slider').find('.slider-count_current')
  const $totalSlides = $('.stock-slider').find('.slider-count_total')
  
  $stockSlider.on('init', function (e, slick) {
    $totalSlides.text(slick.slideCount)
    $currentSlide.text(slick.currentSlide + 1)
  })
  
  $stockSlider.slick({
    speed: 500,
    dots: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '.stock-slider .slider-arrow_prev',
    nextArrow: '.stock-slider .slider-arrow_next',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  })
  
  $stockSlider.on('beforeChange', function (e, slick, current, next) {
    $currentSlide.text(next + 1)
  })
}

function initReviewsSlider() {
  const $reviewsSlider = $('#reviews-slider')
  const $currentSlide = $('.reviews-slider').find('.slider-count_current')
  const $totalSlides = $('.reviews-slider').find('.slider-count_total')
  
  $reviewsSlider.on('init', function (e, slick) {
    $totalSlides.text(slick.slideCount)
    $currentSlide.text(slick.currentSlide + 1)
  })
  
  $reviewsSlider.slick({
    speed: 700,
    dots: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '.reviews-slider .slider-arrow_prev',
    nextArrow: '.reviews-slider .slider-arrow_next',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  })
  
  $reviewsSlider.on('beforeChange', function (e, slick, current, next) {
    $currentSlide.text(next + 1)
  })
}

$(document).ready(function () {
  const $slider = $('.slider')
  const $loader = $('.loader-wrap')
  
  $slider.addClass('d-none')
  
  setTimeout(() => {
    $loader.addClass('hide')
    $slider.removeClass('d-none')
    
    initMainSlider()
    initStockSlider()
    initReviewsSlider()
  }, 2000)
})

$(window).on('load', function() {
  const desc = $('.review-card__desc')
  
  desc.each((index, desc) => {
    const $desc = $(desc)
    const $moreBTn = $(desc).parents('.review-card').find('.review-card__see-more')
    const str = $desc.text()
    
    if (str.length > 220) {
      $moreBTn.removeClass('d-none')
    } else {
      $moreBTn.addClass('d-none')
    }
  })
  
  $('.review-card__see-more').on('click', function() {
    if ($(this).hasClass('opened')) {
      $(this).text('развернуть')
    } else {
      $(this).text('свернуть')
    }
    
    $(this).toggleClass('opened')
    $(this).parents('.review-card').toggleClass('opened')
    $(this).parent().find('.review-card__desc').toggleClass('d-block')
  })
})
