function initStockSlider() {
  const $stockSlider = $('#stock-slider')
  const $currentSlide = $('.stock-slider').find('.slider-count_current')
  const $totalSlides = $('.stock-slider').find('.slider-count_total')

  $stockSlider.on('init', function (e, slick) {
    $totalSlides.text(slick.slideCount)
    $currentSlide.text(slick.currentSlide + 1)
  })

  $stockSlider.slick({
    speed: 500,
    dots: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '.stock-slider .slider-arrow_prev',
    nextArrow: '.stock-slider .slider-arrow_next',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  })

  $stockSlider.on('beforeChange', function (e, slick, current, next) {
    $currentSlide.text(next + 1)
  })
}

function initExampleSlider() {
  $('.example-slider').each(($key, slider) => {
    const $slider = $(slider)
    const $currentSlide = $slider.parent().find('.slider-count_current')
    const $totalSlides = $slider.parent().find('.slider-count_total')

    $slider.on('init', function (e, slick) {
      slick.$slides.each((index, slide) => {
        const $innerSlider = $(slide).find(`.example-inner-slider`)

        $innerSlider.slick({ speed: 600, draggable: false, swipe: false })
      })
      
      $currentSlide.text(slick.currentSlide + 1)
      $totalSlides.text(slick.slideCount <= slick.options.slidesToShow ? 1 : slick.slideCount - 2)
    })

    $slider.slick({
      speed: 600,
      dots: false,
      slidesToShow: 3,
      infinite: false,
      slidesToScroll: 1,
      prevArrow: $slider.parent().find('.slider-arrow_prev').get(0),
      nextArrow: $slider.parent().find('.slider-arrow_next').get(0),
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    })

    $slider.on('beforeChange', function (e, slick, current, next) {
      $currentSlide.text(next + 1)
    })
  })
}

function initConstructorSlider() {
  const $slider = $('.constructor-slider')
  const $sliderWrap = $('.constructor-slider-wrap')
  const $totalSlides = $sliderWrap.find('.slider-count_total')
  const $currentSlide = $sliderWrap.find('.slider-count_current')
  
  $slider.on('init', function (e, slick) {
    $totalSlides.text(slick.slideCount)
    $currentSlide.text(slick.currentSlide + 1)
  })
  
  $slider.slick({
    speed: 500,
    fade: true,
    dots: false,
    slidesToShow: 1,
    infinite: false,
    slidesToScroll: 1,
    prevArrow: '.constructor-slider-wrap .slider-arrow_prev',
    nextArrow: '.constructor-slider-wrap .slider-arrow_next'
  })
  
  $slider.on('beforeChange', function (e, slick, current, next) {
    $currentSlide.text(next + 1)
  })
}

$(document).ready(function () {
  const $slider = $('.slider')
  const $loader = $('.loader-wrap')
  
  $slider.addClass('d-none')
  
  setTimeout(() => {
    $loader.addClass('hide')
    $slider.removeClass('d-none')
  
    initStockSlider()
    initExampleSlider()
    initConstructorSlider()
  }, 2000)
})

$(window).on('load', function() {
  $('.choose-item[data-type]').on('click', function () {
    const type = $(this).attr('data-type')

    $(`.choose-item[data-type=${type}]`).removeClass('active')
    $(this).toggleClass('active')
  })

  $('.choose-item__color').each((index, item) => {
    const color = $(item).attr('data-color')

    $(item).css('background-color', color)
  })
})
