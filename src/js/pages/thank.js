$(document).ready(function () {
  if (window.localStorage.getItem('successType') === 'review') {
    $('#review-success').modal('show')
  } else {
    $('#success').modal('show')
  }
})
