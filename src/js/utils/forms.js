$(document).ready(function() {
  $('.form').on('submit', function(e) {
    const form = $(this)
    const url = form.attr('action')
    const method = form.attr('method')
    const $modal = $(this).parents('.modal')
    const data = $(this).serialize()
    e.preventDefault()
    
    form.find('.form-error').remove()
    
    $.ajax({
      url,
      data,
      method,
      success: function() {
        if ($modal.attr('id') === 'review') {
          window.localStorage.setItem('successType', 'review')
        } else {
          window.localStorage.setItem('successType', 'form')
        }
        
        window.location.href = `${window.location.origin}/thankyou`
      },
      error: function() {
        form.append(`<small class="form-error">Ошибка при отправке формы</small>`)
        
        setTimeout(function() {
          form.find('.form-error').remove()
        }, 2500)
      }
    })
  })
})
