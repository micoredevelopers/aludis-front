function initAOS() {
  AOS.init({
    once: true,
    easing: 'ease',
    duration: 700
  })
}

function initInputMask() {
  const inputsArr = []
  const inputs = document.getElementsByTagName('input')
  
  for (let i = 0; i < inputs.length; i++) {
    if (inputs[i].getAttribute('type') === 'tel') {
      inputsArr.push(inputs[i])
    }
  }
  
  Inputmask({ mask: '+38 (099) 999-99-99' }).mask(inputsArr)
}

function convertImages(query, callback) {
  const images = document.querySelectorAll(query)
  
  images.forEach(image => {
    fetch(image.src)
      .then(res => res.text())
      .then(data => {
        const parser = new DOMParser()
        const svg = parser.parseFromString(data, 'image/svg+xml').querySelector('svg')
        
        if (svg) {
          if (image.id) svg.id = image.id
          if (image.className) svg.classList = image.classList
          image.parentNode.replaceChild(svg, image)
        }
      })
      .then(callback)
      .catch(error => console.error(error))
  })
}

$(window).on('load', function() {
  initAOS()
  initInputMask()
  convertImages('.svg')
  
  $('.global-menu__links-item').on('click', function() {
    $(this).toggleClass('toggle')
    $(this).find('.global-menu__sub-wrap').slideToggle('slow')
  })
})
